/* eslint-env node */
module.exports = {
	extends: ['./.eslintrc.json', 'plugin:@typescript-eslint/recommended'],
	parser: '@typescript-eslint/parser',
	plugins: ['@typescript-eslint'],
	root: true,

	rules: {
		"@typescript-eslint/no-explicit-any": 'off'
	},
};