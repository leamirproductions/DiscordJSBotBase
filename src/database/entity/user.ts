import { Entity, Column, PrimaryColumn, Generated, BaseEntity } from 'typeorm';

export enum UserPermissionFlags {
	NONE = 0,

	// Logs
	SEE_LOGS = 1 << 0,
	DELETE_LOGS = 1 << 1,

	// Dangerous
	EVAL = 1 << 2,

	// Permissions
	GRANT_PERMISSIONS = 1 << 3,
	GRANT_ELEVATED_PERMISSIONS = 1 << 4
}

@Entity()
export class GlobalUser extends BaseEntity {
	@PrimaryColumn()
	@Generated('uuid')
		id: number;

	@Column({
		nullable: false,
		unique: true,
	})
		discordId: string;

	@Column({
		default: UserPermissionFlags.NONE,
		nullable: false,
	})
		permissions: UserPermissionFlags;

	@Column({
		default: false,
		nullable: false,
	})
		isBanned: boolean;

	@Column({
		default: null,
	})
		banExpiry: number;
}
