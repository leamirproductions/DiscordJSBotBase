import { AppDataSource } from '../db-config';
import { GlobalUser, UserPermissionFlags } from './entity/user';

export async function initializeDatabase() {
	await AppDataSource.initialize()
		.then(() => {
			console.log('[typeorm][databases/index.ts] Data Source has been initialized!');
		})
		.catch((err) => {
			throw new Error('[typeorm][databases/index.ts] Error during Data Source initialization\n' + err.toString());
		});
	await AppDataSource.synchronize();
}

export async function waitForDatabaseInitialization() {
	return new Promise<void>((resolve, reject) => {
		const attemptTime = 250;

		let runs = 0;
		function checkInitialized() {
			if (AppDataSource.isInitialized) {return resolve();}

			if (runs > 10) {return reject('Database didn\'t initialize in time!');}

			setTimeout(checkInitialized, attemptTime);
			runs++;
		}
		setTimeout(checkInitialized, attemptTime);
	});
}

export async function setupOwnerRoleIfNotSet() {
	if (process.env.OWNER_DISCORD_ID == '0' || process.env.OWNER_DISCORD_ID == null) {return;}

	if (!AppDataSource.isInitialized) {await waitForDatabaseInitialization();}

	const fullPerms = (function() {
		let combinedValue: UserPermissionFlags = 0;
		for (const perm of Object.values(UserPermissionFlags)) {
			if (typeof perm === 'number' && perm !== UserPermissionFlags.NONE) {
				combinedValue |= perm;
			}
		}
		return combinedValue;
	})();

	const userRepository = AppDataSource.getRepository(GlobalUser);

	const owner = await userRepository.findOne({ where: { discordId: process.env.OWNER_DISCORD_ID } });

	if (owner != null && owner.permissions == fullPerms) {// User perms are already set
		return;
	}

	await userRepository.createQueryBuilder()
		.insert()
		.into(GlobalUser)
		.values({
			discordId: process.env.OWNER_DISCORD_ID,
			permissions: fullPerms,
			isBanned: false,
			banExpiry: null as any,
		})
		.orUpdate(
			['permissions', 'isBanned', 'banExpiry'],
			['discordId'])
		.execute();
	console.log(`[WARNING][database/index.ts] User '${process.env.OWNER_DISCORD_ID}' was given full permissions.`);
}