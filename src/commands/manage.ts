import { ActionRowBuilder, ChatInputCommandInteraction, EmbedBuilder, GuildMember, SlashCommandBuilder, StringSelectMenuBuilder, StringSelectMenuOptionBuilder } from 'discord.js';
import { commandFileClass } from '../helpers/fileClasses';
import { client } from '../util';
import { AppDataSource } from '../db-config';
import { GlobalUser, UserPermissionFlags } from '../database/entity/user';
import { permissionReadableNames } from '../events/manage-perm-search-autocomplete';

export default new commandFileClass(
	new SlashCommandBuilder()
		.setName('manage')
		.setDescription('Altera permissões de um usuário')
		.setDefaultMemberPermissions(0)
		.addSubcommand((subcommand) =>
			subcommand
				.setName('search')
				.setDescription('Busca e lista usuários cadastrados, com permissão de adminsitrador.')
				.addStringOption((option) =>
					option
						.setName('permissions')
						.setDescription('Use \';\' para separar. Permissoes para filtrar')
						.setRequired(false)
						.setAutocomplete(true),
				),
		)
		.addSubcommand((subcommand) =>
			subcommand
				.setName('edit')
				.setDescription('Edita as permissões de um usuário.')
				.addUserOption((option) =>
					option
						.setName('usuario')
						.setDescription('Usuario a ser alterado')
						.setRequired(true),
				),
		),
	async (interaction) => {
		if (interaction.options.getSubcommand() == 'edit') {onEditCommand(interaction);}
		else if (interaction.options.getSubcommand() == 'search') {onSearchCommand(interaction);}
	},
);

function convertFromTextToPermissionFlag(text: string): UserPermissionFlags {
	let decodedPerm = UserPermissionFlags.NONE;

	const parts = text.split(';');

	for (const part of parts) {
		const trimmedPart = part.trim();
		if (trimmedPart in permissionReadableNames) {
			decodedPerm |= permissionReadableNames[trimmedPart as keyof typeof permissionReadableNames];
		}
	}

	return decodedPerm;
}

export function convertFromPermissionFlagToText(flags: UserPermissionFlags): string {
	const readableNames: string[] = [];

	for (const key in permissionReadableNames) {
		const value = permissionReadableNames[key as keyof typeof permissionReadableNames];
		if (value & flags) {readableNames.push(key);}
	}

	return readableNames.join(';');
}

async function onSearchCommand(interaction: ChatInputCommandInteraction) {
	let authorUserDB = await AppDataSource.getRepository(GlobalUser).findOne({
		where: { discordId: interaction.user.id },
	});

	if (!authorUserDB) {authorUserDB = { discordId: interaction.user.id, permissions: UserPermissionFlags.NONE } as GlobalUser;} // Only needed values
	let authorUserPerms = authorUserDB.permissions;

	if ((authorUserPerms & UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS)) {// Elevated perms should superseed lower permission
		authorUserPerms = authorUserPerms | UserPermissionFlags.GRANT_PERMISSIONS;
	}

	if (!(authorUserPerms & UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS)) {return replyWithNoPermsMessage(interaction);}

	const userRequestedPermsString = interaction.options.getString('permissions', false);

	let wantedPerms: UserPermissionFlags = UserPermissionFlags.NONE;
	if (userRequestedPermsString != null) {wantedPerms = convertFromTextToPermissionFlag(userRequestedPermsString);}

	const userSearch = AppDataSource
		.getRepository(GlobalUser)
		.createQueryBuilder()
		.cache({
			id: `admin-search-${interaction.user.id}`,
			miliseconds: 120_000, // 2m
		})
		.where('"GlobalUser"."permissions"  & :wantedPerms = :wantedPerms', { wantedPerms: wantedPerms })
		.andWhere(
			'"GlobalUser"."permissions" != 0',
		);

	const embed = new EmbedBuilder()
		.setTitle('Busca de administrador')
		.setDescription('Usuarios com as permissões espeicifcadas:')
		.setTimestamp();

	const results = await userSearch.getMany();

	for (const usuario of results) {
		embed.addFields({ name: `${(await (client.users.fetch(usuario.discordId))).username}`, value: `${convertFromPermissionFlagToText(usuario.permissions) || 'null'}` });
	}

	if (results.length == 0) {embed.addFields({ name: 'Nenhum usuário encontrado.', value: '\u2008' });}

	interaction.reply({ content: '', embeds: [embed], ephemeral: true });
}

async function onEditCommand(interaction: ChatInputCommandInteraction) {
	let authorUserDB = await AppDataSource.getRepository(GlobalUser).findOne({
		where: { discordId: interaction.user.id },
	});

	if (!authorUserDB) {authorUserDB = { discordId: interaction.user.id, permissions: UserPermissionFlags.NONE } as GlobalUser;} // Only needed values
	let authorUserPerms = authorUserDB.permissions;

	if ((authorUserPerms & UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS)) {// Elevated perms should superseed lower permission
		authorUserPerms = authorUserPerms | UserPermissionFlags.GRANT_PERMISSIONS;
	}

	if (!(authorUserPerms & UserPermissionFlags.GRANT_PERMISSIONS)) {return replyWithNoPermsMessage(interaction);}

	const targetUser = interaction.options.getMember('usuario') as GuildMember | null;
	if (targetUser == null) {
		const responseEmbed = new EmbedBuilder()
			.setColor(0xFF0000)
			.setTitle('Usuário não encontrado!')
			.setDescription('O usuário especificado não foi encontrado.');

		return interaction.reply({ content: '', embeds: [responseEmbed], ephemeral: true });
	}
	let targetUserDB = await AppDataSource.getRepository(GlobalUser).findOne({
		where: { discordId: targetUser.id },
	});

	if (!targetUserDB) {
		targetUserDB = { discordId: targetUser.id, permissions: UserPermissionFlags.NONE } as GlobalUser; // Only needed values
	}
	let targetUserPermissions = targetUserDB.permissions;

	if ((targetUserPermissions & UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS)) {// Elevated perms should superseed lower permission
		targetUserPermissions = targetUserPermissions | UserPermissionFlags.GRANT_PERMISSIONS;
	}

	const userIsOwner = targetUser.id == process.env.OWNER_DISCORD_ID;

	const userIsEditingSelf = targetUserDB.discordId == authorUserDB.discordId;

	const userIsEditingSuperior = (targetUserPermissions & UserPermissionFlags.GRANT_PERMISSIONS) && !(authorUserPerms & UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS); // Users that cant GRANT_ELEVATED_PERMISSIONS can't modify users with GRANT_PERMISSIONS role (or elevated) - Elevated perms users can modify one another, as long as they aren't modifyng the bot owner

	if (userIsOwner || userIsEditingSelf || userIsEditingSuperior) {
		let rejectReason = '';
		if (userIsOwner) {rejectReason += 'O usuário especificado é o desenvolvedor do bot.';}
		if (userIsEditingSelf) {rejectReason += (rejectReason.length > 0 ? '\n' : '') + 'O usuário especificado é você mesmo.';}
		if (userIsEditingSuperior) {rejectReason += (rejectReason.length > 0 ? '\n' : '') + 'O usuário especificado é superior a você.';}

		const responseEmbed = new EmbedBuilder()
			.setColor(0xFF0000)
			.setTitle('Sem permissão!')
			.setDescription('Você não pode alterar este usuário:\n\n' + rejectReason);

		return interaction.reply({ content: '', embeds: [responseEmbed], ephemeral: true });
	}

	const select = new StringSelectMenuBuilder()
		.setCustomId(`manage-perm-selection-${targetUserDB.discordId}`)
		.setMinValues(0)
		.addOptions(
			new StringSelectMenuOptionBuilder()
				.setLabel('Ver logs')
				.setValue('manage-perm-see-logs')
				.setDescription('Permissão para ver logs (listar de uma categoria, e conteudo de todas as logs)')
				.setDefault((targetUserPermissions & UserPermissionFlags.SEE_LOGS) != 0),
			new StringSelectMenuOptionBuilder()
				.setLabel('Deletar logs')
				.setValue('manage-perm-delete-logs')
				.setDescription('Permissão para deletar logs')
				.setDefault((targetUserPermissions & UserPermissionFlags.DELETE_LOGS) != 0),
		);

	if (authorUserPerms & UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS) {
		select
			.addOptions(
				new StringSelectMenuOptionBuilder()
					.setLabel('Dar permissoes')
					.setValue('manage-perm-grant-perms')
					.setDescription('Permissão para dar permissões')
					.setDefault((targetUserPermissions & UserPermissionFlags.GRANT_PERMISSIONS) != 0),
			);
	}

	if (authorUserDB.discordId == (process.env.OWNER_DISCORD_ID)) {
		select
			.addOptions(
				new StringSelectMenuOptionBuilder()
					.setLabel('Dar permissoes elevadas')
					.setValue('manage-perm-grant-elevated-perms')
					.setDescription('Permissão para dar permissões elevadas (dar permissoes, etc)')
					.setDefault((targetUserPermissions & UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS) != 0),
				new StringSelectMenuOptionBuilder()
					.setLabel('Eval expressao')
					.setValue('manage-perm-eval')
					.setDescription('Permissão para utilizar \'eval\' (!!! PERIGOSO !!!)')
					.setDefault((targetUserPermissions & UserPermissionFlags.EVAL) != 0),
			);
	}

	select.setMaxValues(select.options.length);

	const row1 = new ActionRowBuilder<StringSelectMenuBuilder>()
		.addComponents(select);

	await interaction.reply({
		content: 'Selecione as permissões do usuário',
		components: [row1],
		ephemeral: true,
	});
}

function replyWithNoPermsMessage(interaction: ChatInputCommandInteraction) {
	const responseEmbed = new EmbedBuilder()
		.setColor(0xFF0000)
		.setTitle('Sem permissão!')
		.setDescription('Você não tem permissão para utilizar este comando');

	interaction.reply({ content: '', embeds: [responseEmbed], ephemeral: true });
}