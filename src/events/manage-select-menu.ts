import { EmbedBuilder, Events } from 'discord.js';
import { EventFileClass, EventType } from '../helpers/fileClasses';
import { GlobalUser, UserPermissionFlags } from '../database/entity/user';
import { AppDataSource } from '../db-config';
import { LogTypes, writeLog } from '../helpers/logger';
import { convertFromPermissionFlagToText } from '../commands/manage';

const userIdChcekRegex = /^manage-perm-selection-(\d+)$/;

export default new EventFileClass(
	Events.InteractionCreate,
	EventType.on,
	(async interaction => {
		if (!interaction.isStringSelectMenu()) {return;}

		const result = interaction.customId.match(userIdChcekRegex);
		if (result == null) {return;}

		const targetId = result[1];

		// Author
		let authorUserDB = await AppDataSource.getRepository(GlobalUser).findOne({
			where: { discordId: interaction.user.id },
		});

		if (!authorUserDB) {authorUserDB = { discordId: interaction.user.id, permissions: UserPermissionFlags.NONE } as GlobalUser;} // Only needed values
		let authorUserPerms = authorUserDB.permissions;
		if ((authorUserPerms & UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS)) {// Elevated perms should superseed lower permission
			authorUserPerms = authorUserPerms | UserPermissionFlags.GRANT_PERMISSIONS;
		}

		// Target
		let targetUserDB = await AppDataSource.getRepository(GlobalUser).findOne({
			where: { discordId: targetId },
		});

		if (!targetUserDB) {
			targetUserDB = { discordId: interaction.user.id, permissions: UserPermissionFlags.NONE } as GlobalUser; // Only needed values
		}

		let targetUserPermissions = targetUserDB.permissions;
		if ((targetUserPermissions & UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS)) {// Elevated perms should superseed lower permission
			targetUserPermissions = targetUserPermissions | UserPermissionFlags.GRANT_PERMISSIONS;
		}

		const userIsOwner = targetId == process.env.OWNER_DISCORD_ID;
		const userIsEditingSelf = targetId == interaction.user.id;
		const userIsEditingSuperior = (targetUserPermissions & UserPermissionFlags.GRANT_PERMISSIONS) && !(authorUserPerms & UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS); // Users that cant GRANT_ELEVATED_PERMISSIONS can't modify users with GRANT_PERMISSIONS role (or elevated) - Elevated perms users can modify one another, as long as they aren't modifyng the bot owner

		if (userIsOwner || userIsEditingSelf || userIsEditingSuperior) {
			let rejectReason = '';
			if (userIsOwner) {rejectReason += 'O usuário especificado é o desenvolvedor do bot.';}
			if (userIsEditingSelf) {rejectReason += (rejectReason.length > 0 ? '\n' : '') + 'O usuário especificado é você mesmo.';}
			if (userIsEditingSuperior) {rejectReason += (rejectReason.length > 0 ? '\n' : '') + 'O usuário especificado é superior a você.';}

			const responseEmbed = new EmbedBuilder()
				.setColor(0xFF0000)
				.setTitle('Sem permissão!')
				.setDescription('Você não pode alterar este usuário:\n\n' + rejectReason);

			return interaction.reply({ content: '', embeds: [responseEmbed], ephemeral: true });
		}

		const textToPermissionBit = {
			'manage-perm-see-logs': UserPermissionFlags.SEE_LOGS,
			'manage-perm-delete-logs': UserPermissionFlags.DELETE_LOGS,
			'manage-perm-eval': UserPermissionFlags.EVAL,
			'manage-perm-grant-perms': UserPermissionFlags.GRANT_PERMISSIONS,
			'manage-perm-grant-elevated-perms': UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS,
		} as const;

		let finalPerms = UserPermissionFlags.NONE;

		for (const value of interaction.values) {
			// @ts-expect-error - Fuck typescript
			if (!textToPermissionBit[value] == null) {
				const logCode = await writeLog(LogTypes.INTERACTION_ERROR, `Managing user '${targetId}' failed.\n\n\nPermission '${value}' doesn't exist in array. Please correct at \`/events/manage-select-menu.ts\`.`);
				const embed = new EmbedBuilder()
					.setTitle('Oh não!')
					.setDescription(`Ocorreu um erro na execução desta intera;ção.\n\nUma das permissões não foi configurada corretamente, verifique a log.\n\nTente novamente mais tarde.\n\nSe possivel, informe os desenvolvedores.\nInclua o código \`${logCode}\` na sua mensagem.`)
					.setColor('#FF0000')
					.setTimestamp();

				return interaction.reply({ content: '', embeds: [embed], ephemeral: true });
			}

			// @ts-expect-error - TS decides that this is implicity any
			finalPerms = finalPerms | textToPermissionBit[value];
		}

		await AppDataSource.getRepository(GlobalUser).createQueryBuilder()
			.insert()
			.into(GlobalUser)
			.values({
				discordId: targetId,
				permissions: finalPerms,
				isBanned: false,
				banExpiry: null as any,
			})
			.orUpdate(
				['permissions'],
				['discordId'])
			.execute();

		await interaction.reply({ content: `As permissões do usuário <@${targetId}> (${targetId}) foram alteradas para '${convertFromPermissionFlagToText(finalPerms) || 'nenhuma'}'.`, ephemeral: true });
	}),
);