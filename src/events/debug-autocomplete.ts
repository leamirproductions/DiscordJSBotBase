import { Events } from 'discord.js';
import { EventFileClass, EventType } from '../helpers/fileClasses';
import { logPaths } from '../helpers/logger';
import path from 'node:path';
import * as fs from 'node:fs';
import { AppDataSource } from '../db-config';
import { GlobalUser, UserPermissionFlags } from '../database/entity/user';

export default new EventFileClass(
	Events.InteractionCreate,
	EventType.on,
	async function(interaction) {

		if (!interaction.isAutocomplete()) {return;}

		if (interaction.commandName != 'debug') {return;}

		let user = await AppDataSource.getRepository(GlobalUser).findOne({
			where: { discordId: interaction.user.id },
		});

		if (!user) {user = { permissions: UserPermissionFlags.NONE } as GlobalUser;} // Only needed value
		const userPerms = user.permissions;

		if (!(userPerms & (UserPermissionFlags.SEE_LOGS | UserPermissionFlags.DELETE_LOGS))) {return await interaction.respond([]);}

		if (interaction.options.getSubcommand() == 'get-log' || interaction.options.getSubcommand() == 'delete-log' || interaction.options.getSubcommand() == 'get-logs') {
			const focusedOption = interaction.options.getFocused(true);
			if (focusedOption.name == 'category') {
				let values = Object.entries(logPaths)
					.map(([, value]) => value);

				if (focusedOption.value != null) {values = values.filter(choice => choice.startsWith(focusedOption.value));}

				await interaction.respond(values.map(choice => ({ name: choice, value: choice })));
			}
			else if (focusedOption.name == 'code') {
				const currentCategory = interaction.options.getString('category');
				if (currentCategory == null || !Object.values(logPaths).includes(currentCategory)) {return await interaction.respond([]);}

				const finalPath = path.join(__dirname, '../../', 'logs', currentCategory);

				if (!fs.existsSync(finalPath)) {return await interaction.respond([]);}


				let values = fs.readdirSync(finalPath)
					.map(fileName => fileName.replace(/\..+$/, ''));

				if (focusedOption.value != null) {values = values.filter(choice => choice.startsWith(focusedOption.value));}

				return await interaction.respond(values.map(choice => ({ name: choice, value: choice })));
			}
		}
	},
);