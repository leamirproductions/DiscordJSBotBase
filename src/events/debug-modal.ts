import { EmbedBuilder, Events } from 'discord.js';
import { EventFileClass, EventType } from '../helpers/fileClasses';
import { evaluateAndCaptureOutput } from '../util';
import { AppDataSource } from '../db-config';
import { GlobalUser, UserPermissionFlags } from '../database/entity/user';


export default new EventFileClass(
	Events.InteractionCreate,
	EventType.on,
	async (interaction) => {
		if (!interaction.isModalSubmit()) {return;}

		let user = await AppDataSource.getRepository(GlobalUser).findOne({
			where: { discordId: interaction.user.id },
		});

		if (!user) {user = { permissions: UserPermissionFlags.NONE } as GlobalUser;} // Only needed value
		const userPerms = user.permissions;

		if (userPerms & UserPermissionFlags.EVAL) {
			const responseEmbed = new EmbedBuilder()
				.setColor(0xFF0000)
				.setTitle('Sem permissão!')
				.setDescription('Você precisa ser um desenvolvedor do bot para utilizar este comando');

			interaction.reply({ content: '', embeds: [responseEmbed], ephemeral: true });
			return;
		}

		if (interaction.customId == 'interaction-response-debug-eval') {
			await interaction.deferReply({ ephemeral: true });

			const code = interaction.fields.getTextInputValue('debug-eval-modal-arbitrary-code');
			const result = await evaluateAndCaptureOutput(code);

			await interaction.editReply({ files: [result] });
		}
	},
);