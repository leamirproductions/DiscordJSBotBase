import { Events } from 'discord.js';
import { EventFileClass, EventType } from '../helpers/fileClasses';
import { UserPermissionFlags } from '../database/entity/user';

export const permissionReadableNames = {
	'see-logs': UserPermissionFlags.SEE_LOGS,
	'delete-logs': UserPermissionFlags.DELETE_LOGS,

	'eval': UserPermissionFlags.EVAL,

	'grant-perms': UserPermissionFlags.GRANT_PERMISSIONS,
	'grant-elevated-perms': UserPermissionFlags.GRANT_ELEVATED_PERMISSIONS,
} as const;

export default new EventFileClass(
	Events.InteractionCreate,
	EventType.on,
	async function(interaction) {

		if (!interaction.isAutocomplete()) {return;}

		if (interaction.commandName != 'manage') {return;}

		if (interaction.options.getSubcommand() != 'search') {return;}

		const focusedOption = interaction.options.getFocused(true);

		if (focusedOption.name != 'permissions') {return;}

		let currentValues = Array.from(new Set(focusedOption.value.split(';'))); // No duplicates
		if (currentValues[0] == '') {currentValues = [];}

		let permissionsNames = Object.keys(permissionReadableNames) as Array<keyof typeof permissionReadableNames>;

		permissionsNames = permissionsNames.filter(v => !currentValues.includes(v));

		const currElement = currentValues[currentValues.length - 1];
		if (currElement != null && currElement != '') {permissionsNames = permissionsNames.filter(v => v.startsWith(currElement));}

		const finalresponse = permissionsNames.map(choice => ({ name: focusedOption.value + choice.toLowerCase() + ';', value: focusedOption.value + choice.toLowerCase() + ';' }));

		await interaction.respond(finalresponse).catch();
	},
);