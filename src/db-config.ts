import { DataSource } from 'typeorm';
import { GlobalUser } from './database/entity/user';
import { initializeDatabase, setupOwnerRoleIfNotSet } from './database';

export const AppDataSource = new DataSource({ // See https://typeorm.io/data-source-options
	type: 'sqlite',
	database: './discordBot.db',

	cache: true, // See https://orkhan.gitbook.io/typeorm/docs/caching for more options (including redis-like)
	entities: [ GlobalUser ],
});

initializeDatabase();

setupOwnerRoleIfNotSet();